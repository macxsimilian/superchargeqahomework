package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

//WebDriverFactory for SeleniumGrid
public class WebDriverFactory {
    public static WebDriver initDriver(String browserName, String gridURL, String gridPassword) throws MalformedURLException {
        WebDriver driver = null;
        Map<String, Object> preferences = new HashMap<String, Object>();
        preferences.put("enable_do_not_track", "{DISABLE_TRACKING}");
        switch (browserName) {
            case "chrome":
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", preferences);
                driver = new RemoteWebDriver(new URL(gridURL.replace("{PASSWORD}", gridPassword)), options);
                break;
            case "firefox":
                driver = new RemoteWebDriver(new URL(gridURL.replace("{PASSWORD}", gridPassword)), new FirefoxOptions());
                break;
            case "safari":
                driver = new RemoteWebDriver(new URL(gridURL.replace("{PASSWORD}", gridPassword)), new SafariOptions());
                break;
            case "opera":
                driver = new RemoteWebDriver(new URL(gridURL.replace("{PASSWORD}", gridPassword)), new OperaOptions());
                break;
        }
        return driver;
    }
    //WebDriverFactory for Selenium
    public static WebDriver initDriver(String browserName) {
        WebDriver driver = null;
        switch (browserName) {
            case "chrome":
                driver = new ChromeDriver();
                break;
            case "firefox":
                driver = new FirefoxDriver();
                break;
            case "safari":
                driver = new SafariDriver();
                break;
            case "opera":
                driver = new OperaDriver();
                break;

        }
        return driver;
    }
}