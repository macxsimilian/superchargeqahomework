package pages;

import net.bytebuddy.utility.RandomString;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class LoanApplicationFormPage extends MainPage{
    @FindBy(xpath = "//button[contains(text(),'Lähetä hakemus')]")
    private WebElement sendYourApplication_button;
    @FindBy(xpath = "//input[@id='applicant.email']")
    private WebElement emailInputField;
    @FindBy(xpath = "//label[@id='doesAgreeTerms_label']")
    private WebElement doesAgreeTerms_label;
    @FindBy(xpath = "//button[@id='onboarding-form-button-step-next']")
    private WebElement onboardingForm_next_button;
    @FindBy(xpath = "//input[@id='applicant.ssn']")
    private WebElement ssnInputField;
    @FindBy(xpath = "//input[@id='applicant.firstName']")
    private WebElement firstNameInputField;
    @FindBy(xpath = "//input[@id='applicant.lastName']")
    private WebElement lastNameInputField;

    public LoanApplicationFormPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }
    public void fillEmailInput(String email_address){
        RandomString randomStirng = new RandomString();
        String emailAddress = email_address + "+" + randomStirng;
        emailInputField.clear();
        emailInputField.sendKeys(emailAddress);
    }

    public void clickAgreeTermsLabel(){
        doesAgreeTerms_label.clear();
        doesAgreeTerms_label.click();
    }

    public void clickNextButton(){
        onboardingForm_next_button.clear();
        onboardingForm_next_button.click();
    }

    public void fillSsnNo(String ssn_no){
        ssnInputField.clear();
        ssnInputField.sendKeys(ssn_no);
    }

    public void fillFirstName(String firstname){
        firstNameInputField.clear();
        firstNameInputField.sendKeys(firstname);
    }

    public void fillLastName(String lastname){
        lastNameInputField.clear();
        lastNameInputField.sendKeys(lastname);
    }
}
