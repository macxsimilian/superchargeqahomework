package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class MainPage extends BasePage {
    @FindBy(xpath = "//div[contains(text(),'Tietoa Sortterista')]")
    private WebElement aboutSortter_button;
    @FindBy(xpath = "//a[@id='header-link-dropdown-link-3-0'][contains(text(),'Tietoa meistä')]")
    private WebElement informationAboutUs_button;
    @FindBy(id = "company-link-onboarding")
    private WebElement getLoanOffers_button;

    public MainPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
    }

    public void clickAboutSortterButton(){
        aboutSortter_button.click();
    }

    public void clickAboutUsButton(){
        informationAboutUs_button.click();
    }

    public void clickGetLoanOfferButton(){
        getLoanOffers_button.click();
    }
}
