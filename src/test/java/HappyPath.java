import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import pages.BasePage;
import pages.LoanApplicationFormPage;
import pages.MainPage;


public class HappyPath extends BaseTest {
    public static MainPage mainPage;
    public static LoanApplicationFormPage loanApplicationFormPage;

    @BeforeAll
    public static void setUp() {
        BaseTest.setUp();
        mainPage = new MainPage(driver);
        loanApplicationFormPage = new LoanApplicationFormPage(driver);
        mainPage.navigateToUrl(DISABLE_TRACKING_URL);
    }

    @Test
    public void happyPath() {
        mainPage.clickAboutSortterButton();
        mainPage.clickAboutUsButton();
        mainPage.clickGetLoanOfferButton();
        loanApplicationFormPage.clickNextButton();
        loanApplicationFormPage.fillEmailInput(onboarding_email_address);
        loanApplicationFormPage.clickAgreeTermsLabel();
        loanApplicationFormPage.clickNextButton();
        loanApplicationFormPage.fillFirstName(firstname);
        loanApplicationFormPage.fillLastName(lastname);
        loanApplicationFormPage.fillSsnNo(ssn_no);
        loanApplicationFormPage.clickNextButton();

    }

    @AfterAll
    public static void teardown(){
    }
}
