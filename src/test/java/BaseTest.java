import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;
import util.WebDriverFactory;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected static final String BROWSER = System.getenv("BROWSER");
    protected static final String BASEURL = System.getenv("BASEURL");
    protected static final String DISABLE_TRACKING_URL = System.getenv("DISABLE_TRACKING_URL");
    protected static final String onboarding_email_address = System.getenv("ONBOARDING_EMAIL_ADDRESS");
    protected static final String ssn_no = System.getenv("SSN_NO");
    protected static final String firstname = System.getenv("FIRST_NAME");
    protected static final String lastname = System.getenv("LAST_NAME");
    protected static WebDriver driver;

    @BeforeAll
    public static void setUp() {
        driver = WebDriverFactory.initDriver(BROWSER);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterAll
    public static void tearDown() {
        driver.quit();
    }
}
